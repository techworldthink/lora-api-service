from django.shortcuts import render, redirect, get_object_or_404
from rest_framework.response import Response
from django.http import JsonResponse
from rest_framework import status
from django .contrib import messages
from django .contrib.auth.models import User
from rest_framework.decorators import api_view
from rest_framework.decorators import permission_classes
from rest_framework.decorators import authentication_classes
from .helpers import *
from .models import ApiPermission
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from rest_framework.authtoken.models import Token
from .models import ApiAccessHistory
from .forms import ApiForm
from .forms import DeviceSetForm
from .forms import ApiPermissionForm
from .forms import InfluxQueryForm


##################################################################################
#                                                                                #
#                               PUBLIC API HANDLER                               # 
#                                                                                #
##################################################################################
@api_view(['GET'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def api_handler_default(request,app_name,time_frame="",period=0,from_="",to_=""):
    return api_handler(request,app_name,time_frame,period,from_,to_)


##################################################################################
#                                                                                #
#                                 VIEWS  - API                                   # 
#                                                                                #
##################################################################################

# This view retrieves all API from databse and also create new API
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def api_manage(request):
    if request.method == 'POST':
        form = ApiForm(request.POST)
        if form.is_valid():
            # Check if the API name already exists
            api_name = form.cleaned_data['api_name']
            if not Api.objects.filter(api_name=api_name).exists():
                form.save()
                messages.success(request, "Success! New API has been created.")
            else:
                messages.warning(request, f"Sorry, we are unable to create an API with the name {api_name}. Please choose a different name that is not already in use.")
    else:
        form = ApiForm()

    # Get all existing APIs
    apis = Api.objects.all()
    base_url = get_system_url()

    return render(request, "apiapp/manage_api.html", {
        'apis': apis,
        'base_url': base_url,
        'form': form,  # Pass the form to the template
    })

# update api data
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def api_update(request):
    api = get_object_or_404(Api, pk=request.POST['api_id'])
    
    if request.method == 'POST':
        form = ApiForm(request.POST, instance=api)
        if form.is_valid():
            api.api_desc = form.cleaned_data['api_desc']
            api.save(update_fields=["api_desc"])
            messages.success(request, "Success! API has been updated.")
        else:
            messages.error(request, "An error occurred in API update. Please try again.")
        return redirect('adminapp:apiapp:api_manage')

    return redirect('adminapp:apiapp:api_manage')



# delete api with a given api-id
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def api_delete(request,api_id):
    try:
        id = api_id
        api = Api.objects.get(pk=id)
        api.delete()
        messages.info(request, "Success! API deleted. ")
    except:
        messages.error(request,"An error occurred in API deletion. Please try again.")

    return redirect('adminapp:apiapp:api_manage')


# toggle current status of an api
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def api_status_toggle(request,api_id):
    try:
        api= Api.objects.get(pk=api_id)
        api.api_status = not api.api_status
        api.save()
        messages.success(request, "Success! API status has been  updated.")
    except:
        messages.error(request,"An error occurred in API  status toggle. Please try again.")

    return redirect('adminapp:apiapp:api_manage')


##################################################################################
#                                                                                #
#                                 VIEWS  - Devicesets                            # 
#                                                                                #
##################################################################################

# to create device set
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def devicesets_manage(request):
    if request.method == 'POST':
        form = DeviceSetForm(request.POST)
        if form.is_valid():
            api_id = form.cleaned_data['ds_apis_id'].id
            device_name = form.cleaned_data['ds_name']

            if DeviceSet.objects.filter(ds_apis_id=api_id, ds_name=device_name).exists():
                messages.warning(request, "Device set already exists!")
            else:
                form.save()
                messages.success(request, "Device set created.")
                
        else:
            messages.warning(request, "Invalid form data. Please check the details.")

    else:
        form = DeviceSetForm()

    device_objects = DeviceSet.objects.all()
    device_set = []
    for device in device_objects:
        apiObj = Api.objects.get(id=device.ds_apis_id.id)
        device_set.append({
            'ds_id': device.id,
            'ds_name': device.ds_name,
            'latitude': device.latitude,
            'longitude': device.longitude,
            'altitude': device.altitude,
            'api_id': apiObj.id,
            'api_name': apiObj.api_name,
            'is_combined':device.is_combined
        })

    return render(request, "apiapp/manage_devicesets.html", {
        'apis': Api.objects.all(),
        'device_set': device_set,
        'form': form
    })

 
# update deviceset
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def devicesets_update(request):
    deviceset = get_object_or_404(DeviceSet,pk=request.POST.get('ds_id'))
    if request.method == 'POST':
        form = DeviceSetForm(request.POST, instance=deviceset)
        if form.is_valid():
            deviceset.ds_name = form.cleaned_data['ds_name']
            deviceset.latitude = form.cleaned_data['latitude']
            deviceset.longitude = form.cleaned_data['longitude']
            deviceset.altitude = form.cleaned_data['altitude']
            deviceset.is_combined = form.cleaned_data['is_combined']
            print(form.cleaned_data['is_combined'])
            deviceset.save(update_fields=["ds_name","latitude","longitude","altitude","is_combined"])
            messages.success(request, "Success! Device Set has been updated.")
            print(deviceset.is_combined)
        else:
            messages.error(request, "An error occurred in Device set update. Please try again.")
    return redirect('adminapp:apiapp:devicesets_manage')


# delete a device set object with given deviceset-id
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def delete_deviceset(request,id):
    try:
        deviceset = DeviceSet.objects.get(pk=id)
        deviceset.delete()
        messages.info(request,"Success! Device set has been removed")
    except:
        messages.warning(request,"An error occurred in device set deletion. Please try again.")

    return redirect('adminapp:apiapp:devicesets_manage')


##################################################################################
#                                                                                #
#                                 VIEWS  - API Permissions                       # 
#                                                                                #
##################################################################################


# get all permission details of users on apis
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def permissions(request):
    if request.method == 'POST':
        form = ApiPermissionForm(request.POST)
        if form.is_valid():
            user_id = form.cleaned_data['user'].id
            api_id = form.cleaned_data['api'].id
            try:
                # check if permission already exists
                permission_exists = ApiPermission.objects.filter(api=api_id, user=user_id).exists()
                if permission_exists:
                    messages.warning(request, "This user already has permission for this selected API.")
                else:
                    form.save()
                    messages.success(request, "Success! API permission granted.")
            except Exception as e:
                messages.warning(request, "An error occurred in granting permission. Please try again.")
    else:
        form = ApiPermissionForm()

    permission_list = ApiPermission.objects.values_list('api__api_name', 'user__username', 'id')
    users = User.objects.values_list('id', 'username')
    apis = Api.objects.values_list('id', 'api_name')

    return render(request, "apiapp/permissions.html", {
        "permissions": permission_list,
        "users": users,
        "apis": apis,
        "form": form,
    })




# delete permission
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def delete_permissions(request,id):
    try:
        # if permission exist, delete it.
        permissions = ApiPermission.objects.get(id=id)
        permissions.delete()
        messages.info(request,"Success! Permission has been removed.")
    except:
        messages.error(request,"An error occurred in granting permission. Please try again.")
    
    return redirect('adminapp:apiapp:permissions')


##################################################################################
#                                                                                #
#                                 VIEWS  - Tokens                                # 
#                                                                                #
##################################################################################

# get tokens of users
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def tokens(request):
    users = User.objects.all()
    token_list = []
    for each in users:
        try:
            token = Token.objects.get(user=each)
            token_list.append([each.username,token.key,each.id])
        except Exception as e:
            token_list.append([each.username,"-",each.id])

    return render(request,"apiapp/token.html",{
        "tokens" : token_list
    }) 


# generate user authentication token 
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def token_generate(request,id):
    try:
        user = User.objects.get(pk=id)
        # token regenerate
        if(Token.objects.filter(user=user).exists()):
            token = Token.objects.get(user=user)
            token.delete()
            Token.objects.create(user=user)
            messages.success(request,"Success! Token has regenerated.")
        # token create first time
        else:
            token = Token.objects.create(user=user)
            messages.success(request,"Success! Token has been generated.")
    except Exception as e:
        messages.error(request,"An error occurred in token generation. Please try again.")
    return redirect('adminapp:apiapp:tokens')


##################################################################################
#                                                                                #
#                                 VIEWS  - API Build                             # 
#                                                                                #
##################################################################################



# This view retrieves all API from database to build api
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def api_build(request):
    apis = Api.objects.all()
    return render(request,"apiapp/build_api.html",{
        'apis' : apis
    }) 

# This view build an API
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def build_api(request):
    if request.method == "POST":
        form = InfluxQueryForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)

            datasource_id = request.POST['query_datasource']
            try:
                datasource_obj = DataSource.objects.get(pk=datasource_id)
            except DataSource.DoesNotExist:
                form.add_error('query_datasource', 'Datasource does not exist!')
                return JsonResponse({'url': 'Error', 'message': 'Datasource does not exist!'})

            base_url = get_system_url()

            api_id = request.POST['api_id']
            try:
                api_obj = Api.objects.get(pk=api_id)
            except Api.DoesNotExist:
                form.add_error('query_api', 'API does not exist!')
                return JsonResponse({'url': 'Error', 'message': 'API does not exist!'})

            device_id = request.POST['query_deviceset']
            try:
                deviceset_obj = DeviceSet.objects.get(id=device_id)
            except DeviceSet.DoesNotExist:
                form.add_error('query_deviceset', 'Deviceset does not exist!')
                return JsonResponse({'url': 'Error', 'message': 'Deviceset does not exist!'})

            instance.query_datasource = datasource_obj
            instance.query_fields = "[" + instance.query_fields + "]"
            instance.query_where = "[" + instance.query_where.replace(" & ", ", ") + "]"
            instance.query_deviceset = deviceset_obj
            instance.save()

            return JsonResponse({'url': base_url + api_obj.api_name,
                'message': ''})
        else:
            errors = {field: form.errors[field][0] for field in form.errors}
            print(errors)
            return JsonResponse({'url': 'Invalid data', 'message': errors})
    else:
        return JsonResponse({'message': 'Invalid HTTP method'})


def is_deviceset_exist(device_name):
    try:
        device_name,map_id = device_name.rsplit('_',1)
        devicesets = list(DeviceSet.objects.filter(ds_name = device_name, id=map_id))
        if(len(devicesets) < 1):
            return 0
    except:
        return 0
    return [device_name,map_id]



# Retrieves all queries 
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def querysets(request):
    api_id = request.GET['api_id']
    try:
        # get api object
        api = Api.objects.get(pk=api_id)
        # get api-device set list corresponding to a given api
        device_set = DeviceSet.objects.filter(ds_apis_id=api)
        device_list = list(device_set)
        # get all device set corresponding to given api
        device_sets_ids = [each.id for each in device_set]
        # get all queries corresponding to a given device set
        deviceset_queries = InfluxQuery.objects.filter(query_deviceset__in = device_sets_ids)
        # fetch all id of queries that matches with given list of device set
        queries_ids = [each.id for each in deviceset_queries]
        # list of all queries 
        #queries = list(InfluxQuery.objects.filter(id__in=queries_ids).values())
        queries = InfluxQuery.objects.filter(id__in=queries_ids)
        queries_res = []
        for query in queries:
            query_data = {
                'query_data_name': query.query_data_name,
                'query_measurement': query.query_measurement,
                'query_fields': query.query_fields,
                'query_where': query.query_where,
                'query_limit': query.query_limit,
                'device_set_name': query.query_deviceset.ds_name if query.query_deviceset else None,
                'device_set_id': query.query_deviceset.id if query.query_deviceset else None,
                'id' : query.id
            }
            queries_res.append(query_data)

        # Sort queries_res by device_set_name
        queries_res_sorted = sorted(queries_res, key=lambda x: x['device_set_name'])

        return JsonResponse({
            'querysets' : queries_res_sorted,
        })
        
    except Exception as e:
        print(e)
        messages.warning(request,"Error in fetching query data!")

    return JsonResponse({ })


# delete a query object with given query-id 
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def delete_queries(request,id):
    try:
        query = InfluxQuery.objects.get(pk=id)
        query.delete()
        messages.info(request,"Success! Query has been removed")
    except:
        messages.warning(request,"An error occurred in query deletion. Please try again.")

    return redirect('adminapp:apiapp:buildapi')




##################################################################################
#                                                                                #
#                                   AJAX CALLS                                   # 
#                                                                                #
##################################################################################

# Retrieves all devicesets corresponding to an API
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def devicesets(request):
    api_id = request.GET['api_id']
    res = []
    try:
        api_object = Api.objects.get(pk=api_id)
        device_sets = get_device_sets(api_object)
        for device in device_sets:
            device_name = device['ds_name']
            res.append(device_name+"_"+str(device['id']))
    except Api.DoesNotExist:
        messages.warning(request,"Device sets not exist.")

    return JsonResponse({"devicesets":res})

# Retrieves API details
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def apis(request):
    api_list = list(Api.objects.values_list('id','api_name'))
    return JsonResponse({"apis":api_list})

# Retrives all datasource values
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def dataSources(request):
    datasources_list = list(DataSource.objects.values_list('id','db_name'))
    return JsonResponse({"datasources":datasources_list})

# Retrives all measurement names for a particular datasource
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def measurements(request):
    datasource_id = request.GET['datasource_id']
    measurements_list = getAllMeasurements(datasource_id)
    return JsonResponse({"measurements":measurements_list})

# Retrives all fields names for a particular measurement
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def fields(request):
    datasource_id = request.GET['datasource_id']
    measurement_name = request.GET['measurement_name']
    field_list = getAllfields(datasource_id,measurement_name)
    return JsonResponse({"fields":field_list})

# Retrives all distinct values for a particular field
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def distinctvalues(request):
    datasource_id = request.GET['datasource_id']
    measurement_name = request.GET['measurement_name']
    field_name = request.GET['field_name']
    distinct_values = getAllDistictValues(datasource_id,measurement_name,field_name)
    return JsonResponse({"distictvalues":distinct_values})

