from .models import Api
from .models import ApiUrl
from datasourcesapp.models import DeviceSet
from datasourcesapp.models import InfluxQuery
from datasourcesapp.models import DataSource
from apiapp.models import ApiPermission
from influxdb import InfluxDBClient
from rest_framework.response import Response
from .models import ApiAccessHistory
from rest_framework import status
import datetime
from dateutil import parser




##################################################################################
#                                                                                #
#                                   API Handlers                                 # 
#                                                                                #
##################################################################################

def api_handler(request,app_name,time_frame,period,from_,to_):
    result = {}
    # get api object using application name
    api_object = get_api_object(app_name) 

    # authorized ?
    if(not is_authorized(request,api_object)):
        save_history(api_object,request.user,0)
        return Response(status.HTTP_401_UNAUTHORIZED)
    # save history
    try:
        save_history(api_object,request.user,1)
    except Exception as e:
        pass
    
    # Handle wrong url / application name   
    if(api_object is None):
        return Response(status.HTTP_404_NOT_FOUND)

    # if api is disabled
    if(api_object.api_status == 0):
        return Response({},status.HTTP_200_OK)

    # get device sets
    device_sets = get_device_sets(api_object)
    for device in device_sets:
        # is combined ? 
        is_combined=device['is_combined']
        # fetch device set name
        device_name = device['ds_name']
        if device_name is not None:
            if device_name not in result:
                #result[device_name] = []
                result[device_name] = {}
            result[device_name]["location"] = {
                "latitude" : device['latitude'],
                "longitude" : device['longitude'],
                "altitude" : device['altitude']
            }
            if(is_combined):
                result[device_name]["data"] = get_sensors_data_combined(device['id'],time_frame,period,from_,to_,is_combined)
            else:
                result[device_name]["data"] = get_sensors_data_uncombined(device['id'],time_frame,period,from_,to_,is_combined)
    return Response(result,status.HTTP_200_OK)

# Check to see if the user has access to the corresponding API.
def is_authorized(request,api_object):
    user = request.user.id
    # Obtain a list of the corresponding API's permitted users. 
    users =  list(ApiPermission.objects.filter(api=api_object).values_list('user'))
    for each in users:
        if(each[0] == user):
            return True
    return False


# api history save
def save_history(api_object,user,is_authorized):
    api_history = ApiAccessHistory()
    api_history.history_api = api_object
    api_history.history_user = user
    api_history.history_isauthorized = is_authorized
    api_history.save()


# find an API object by using the application name identifier.
def get_api_object(app_name):
    try:
        return Api.objects.get(api_name=app_name)
    except Api.DoesNotExist:
        return None

# get device set of given API.
def get_device_sets(api_object):
    return list(DeviceSet.objects.filter(ds_apis_id=api_object).values())

# get device name
def get_device_name(device_sets):
    try:
        return DeviceSet.objects.get(id=device_sets).ds_name
    except DeviceSet.DoesNotExist:
        return None

#  get data from database   
def get_sensors_data_uncombined(device_id,time_frame,period,from_,to_,is_combined):
    result = {}
    # get list of dataset-query  mapper objects
    dataset_queries_list  = get_dataset_queries_objects(device_id)
    # iterate over dataset-query mappers and fetch corresponding queries object
    for queries in dataset_queries_list:
        query_obj = get_query_object(queries['id'])
        # if query doesn't exist, leave it
        if query_obj is None:
            continue
        # if query exist, fetch data by executing query
        query_data = get_query_data(query_obj,time_frame,period,from_,to_,is_combined)
        
        if query_data is not None:
            # if query success, store the data in a dictionary
            result[query_obj.query_data_name] = query_data 
    return result

def get_sensors_data_combined(device_id,time_frame,period,from_,to_,is_combined):
    result = {}
    # get list of dataset-query  mapper objects
    dataset_queries_list  = get_dataset_queries_objects(device_id)

    # Combine the results based on datetime
    combined_data = {}

    # iterate over dataset-query mappers and fetch corresponding queries object
    for queries in dataset_queries_list:
        query_obj = get_query_object(queries['id'])
        # if query doesn't exist, leave it
        if query_obj is None:
            continue
        # if query exist, fetch data by executing query
        query_data = get_query_data(query_obj,time_frame,period,from_,to_,is_combined)

        for point in query_data.get_points():
            time = point['time']  # Assuming 'time' is the common field
            combined_data.setdefault(time, {}).update(
                {query_obj.query_data_name if key == 'value' else key: value for key, value in point.items() if key != 'time'}
                )
    return combined_data

# get dataset-query mapper objects
def get_dataset_queries_objects(device_id):
    return list(InfluxQuery.objects.filter(query_deviceset=device_id).values())

# get query object by id, if exist
def get_query_object(queries_id):
    try:
        return InfluxQuery.objects.get(id=queries_id)
    except InfluxQuery.DoesNotExist:
        return None

# get datasource details by id, if exist
def get_datasource(datasource_id):
    try:
        datasource =  DataSource.objects.get(id=datasource_id)
        if(datasource.db_status == 0):
            return None
        else:
            return datasource
    except DeviceSet.DoesNotExist:
        return None

# query execution
def get_query_data(query_obj,time_frame,period,from_,to_,is_combined):
    datasource = query_obj.query_datasource
    measurement = query_obj.query_measurement
    # [field_1,field_2]
    query_fields = query_obj.query_fields
    # [[field_1,">",value]]
    query_where = query_obj.query_where
    query_limit= query_obj.query_limit
    # get datasource object
    datasource_obj = get_datasource(datasource.id)
    # if datasource doesn't find, leave it.
    if datasource_obj is None:
        return None
    # if datasource is disabled, leave it
    if datasource_obj.db_status == 0:
        return None
    # get influx db client
    influx_client = get_db_client(datasource_obj)
    # generate query
    query = generate_query(measurement,query_fields,query_where,query_limit,time_frame,period,from_,to_)
    # execute query
    query_result = execute_query(influx_client,query,is_combined)
    
    return query_result

# Database connections
def get_db_client(conn):
    client = InfluxDBClient(host=conn.db_host,port=conn.db_port,username=conn.db_user,password=conn.db_pass)
    client.switch_database(conn.db_name)
    return client

# generate query
def generate_query(measurement,query_fields,query_where,query_limit,time_frame,period,from_,to_):
    query = "SELECT "
    query = query + generate_fields(query_fields.strip('][').split(', '))
    query = query + " FROM "
    query = query + measurement
    if(query_where != "" and query_where != "[]"):
        query = query + " WHERE "
        query = query + generate_where_clauses(query_where.strip('][').split(', '))
    if(time_frame != "" and period !=""):
        # add a 'AND' , if already a where condition present
        if(query_where != "" and query_where != "[]"):
            query = query + " AND "
        # fetch data within the time frame
        query = query + time_frame_query(time_frame,period)
        query = query + " ORDER BY time DESC "
    elif(from_ != "" and to_ != ""):
        # fetch data between given time period
        query_ = time_between_query(from_,to_)
        if(query_ != ""):
            # add a 'AND' , if already a where condition present
            if(query_where != "" and query_where != "[]"):
                query = query + " AND "

            query = query + query_
            query = query + " ORDER BY time DESC "
    # limit = -1 , means there is no limit
    elif(query_limit !=-1):
        query = query + " ORDER BY time DESC LIMIT "
        query = query + str(query_limit)
    
    print(query)
    return query

# generate fields value and seperate by comma
def generate_fields(query_fields):
    return ','.join(query_fields)

# generate where condition query for a given time period
# minutes,hours,days,weeks,months,years 
def time_frame_query(time_frame,period):
    time_range = get_timedate_range_(time_frame,period)
    return "time <= '" + time_range[0] + "' AND time >= '" + time_range[1] + "'"

# generate where conditions
def generate_where_clauses(query_where):
    result = ""
    # length of query_where list
    query_length = len(query_where)
    iteration = 1
    while(query_length // 3 >= iteration):
        if(iteration != 1):
            result = result + " AND "
        # get where condition symbol
        symbol = get_symbol(query_where[(iteration * 3)-2])
        result = result + query_where[(iteration * 3)-3]
        result = result + symbol[1]
        if(symbol[0] == 1):
            result = result + "'" +query_where[(iteration * 3)-1] + "'"
        else:
            result = result +query_where[(iteration * 3)-1]
        iteration = iteration + 1
    return result

# get symbol for where conditions
def get_symbol(symbol):
    if(symbol == "eq"):
        return [1,"="]
    if(symbol == "noteq"):
        return [1,"!="]
    if(symbol == "gt"):
        return [0,">"]
    if(symbol == "lt"):
        return [0,"<"]
    if(symbol == "gteq"):
        return [0,">="]
    if(symbol == "lteq"):
        return [0,"<="]
    return None

# Query exection
def execute_query(influx_client,query_,is_combined):
    data  = influx_client.query(query_)
    if(is_combined):
        return data
    return list(data.get_points())

# get time ranges based of time_frame
# if time_frame type is wrong, return all data
def get_timedate_range_(time_frame,period):
    if(time_frame == "minutes"):
        return get_timedate_range(minutes=period,hours=0,days=0,weeks=0,months=0,years=0)
    elif(time_frame == "hours"):
        return get_timedate_range(minutes=0,hours=period,days=0,weeks=0,months=0,years=0)
    elif(time_frame == "days"):
        return get_timedate_range(minutes=0,hours=0,days=period,weeks=0,months=0,years=0)
    elif(time_frame == "weeks"):
        return get_timedate_range(minutes=0,hours=0,days=0,weeks=period,months=0,years=0)
    elif(time_frame == "months"):
        return get_timedate_range(minutes=0,hours=0,days=0,weeks=0,months=period,years=0)
    elif(time_frame == "years"):
        return get_timedate_range(minutes=0,hours=0,days=0,weeks=0,months=0,years=period)
    else:
        return get_timedate_range(minutes=0,hours=0,days=0,weeks=0,months=0,years=0)
    
# get previous datetime with format (yyyy-MM-ddTHH:mm:ss.fZ)
def get_timedate_range(minutes=0,hours=0,days=0,weeks=0,months=0,years=0 ):
    if(years != 0):
        months = years * 12
    if(months != 0 ):
        days = months * 30
    crnt_time = datetime.datetime.now(datetime.timezone.utc)
    compare_time = crnt_time - datetime.timedelta(minutes = minutes,hours=hours,days=days,weeks=weeks)
    previous_date = compare_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    crnt_date = crnt_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    return [crnt_date,previous_date]

# check weather timestrings are in ISO 8601 format
# if yes return the generated query part
# else return null string
def time_between_query(from_,to_):
    try:
        # Try to convert the string to a datetime object
        # date = datetime.datetime.fromisoformat(from_)
        # date = datetime.datetime.fromisoformat(to_)
        date = parser.isoparse(from_)
        date = parser.isoparse(to_)
        return " time >= '" + from_ + "' AND time <= '" + to_ + "' "
    except Exception as e:
        print(e)
        return ""

##################################################################################
#                                                                                #
#                                 View  Helpers                                  # 
#                                                                                #
##################################################################################

# get system base url from database
# if not set, return demo url
def get_system_url():
    try:
        system_url = ApiUrl.objects.first()
        url = system_url.system_base_url + system_url.system_api_prefix
        return url
    except Exception as e:
        pass
    
    return "http://0.0.0.0/"


# get  all measurements of selected datasource
def getAllMeasurements(datasource_id):
    try:
        datasource_obj = DataSource.objects.get(pk=datasource_id)
        db_client = get_db_client(datasource_obj)
        measurements = get_measurements(db_client,datasource_obj.db_name)
        return measurements
    except DataSource.DoesNotExist:
        return []

# get all fields of selected measurement
def getAllfields(datasource_id,measurement_name):
    try:
        datasource_obj = DataSource.objects.get(pk=datasource_id)
        db_client = get_db_client(datasource_obj)
        fields = get_fields(db_client,datasource_obj.db_name,measurement_name)
        return fields
    except DataSource.DoesNotExist:
        return []

# get all distinct values of a field within a measurement
def getAllDistictValues(datasource_id,measurement_name,field_name):
    try:
        datasource_obj = DataSource.objects.get(pk=datasource_id)
        db_client = get_db_client(datasource_obj)
        fields = get_distinct_values(db_client,datasource_obj.db_name,measurement_name,field_name)
        return fields
    except DataSource.DoesNotExist:
        return []




##################################################################################
#                                                                                #
#                                 Db Helpers                                     # 
#                                                                                #
##################################################################################

# get all measuremnts from datasource
def get_measurements(db_client,db_name):
    db_client.switch_database(db_name)
    data = db_client.query("SHOW MEASUREMENTS")
    result = list(data.get_points())
    measurements = [each['name'] for each in result]
    return measurements

# get all fields of selected measurement.
def get_fields(db_client,db_name,measurement_name):
    db_client.switch_database(db_name)
    fkeys_data = db_client.query("SHOW FIELD KEYS FROM "+measurement_name)
    fkeys_data = list(fkeys_data.get_points())
    fkeys_data_ = [each['fieldKey'] for each in fkeys_data]
    tkeys_data = db_client.query("SHOW TAG KEYS FROM "+measurement_name)
    tkeys_data = list(tkeys_data.get_points())
    tkeys_data_ = [each['tagKey'] for each in tkeys_data]
    result = fkeys_data_ + tkeys_data_
    print(result)
    return result

# get all distinct value of a selected field within a measurement. 
def get_distinct_values(db_client,db_name,measurement_name,field_name):
    db_client.switch_database(db_name)
    data = db_client.query("SELECT distinct("+ field_name +") FROM (SELECT "+ field_name +",value FROM " + measurement_name + ");")
    result = list(data.get_points())
    result_ = [each['distinct'] for each in result]
    return result_