from django import forms
import re

from datasourcesapp.models import DeviceSet
from datasourcesapp.models import InfluxQuery
from .models import Api
from .models import ApiPermission




class ApiForm(forms.ModelForm):
    class Meta:
        model = Api
        fields = ['api_name', 'api_desc']

    def clean(self):
        cleaned_data = super().clean()
        api_name = cleaned_data.get('api_name')
        api_desc = cleaned_data.get('api_desc')

        if not re.match(r'^[a-zA-Z0-9_]+$', api_name):
            self.add_error('api_name', "API name can only contain alphabets, numbers, and underscores.")

        if not re.match(r'^[a-zA-Z0-9\s]+$', api_desc):
            self.add_error('api_desc', "API description can only contain alphabets, numbers and spaces.")

        return cleaned_data
    



class DeviceSetForm(forms.ModelForm):
    class Meta:
        model = DeviceSet
        fields = ['ds_name', 'ds_apis_id', 'latitude', 'longitude', 'altitude','is_combined']

        def clean(self):
            cleaned_data = super().clean()
            ds_name = cleaned_data.get('ds_name')
            latitude = cleaned_data.get('latitude')
            longitude = cleaned_data.get('longitude')
            altitude = cleaned_data.get('altitude')
            is_combined = cleaned_data.get('is_combined')

            if not re.match(r'^[a-zA-Z0-9_]+$', ds_name):
                self.add_error('ds_name', "Device set name can only contain alphabets, numbers, and underscores.")

            if not re.match(r'^[0-9\s]+$', latitude):
                self.add_error('latitude', "Latitude can only contain numbers.")
            if not re.match(r'^[0-9\s]+$', longitude):
                self.add_error('longitude', "Longitude can only contain numbers.")
            if not re.match(r'^[0-9\s]+$', altitude):
                self.add_error('altitude', "Altitude can only contain numbers.")

            return cleaned_data


class ApiPermissionForm(forms.ModelForm):
    class Meta:
        model = ApiPermission
        fields = ['user', 'api']

class InfluxQueryForm(forms.ModelForm):
    class Meta:
        model = InfluxQuery
        fields = ['query_data_name', 'query_datasource', 'query_measurement', 'query_fields', 'query_where', 'query_limit', 'query_deviceset']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Set query_where field as not required
        self.fields['query_where'].required = False

    def clean_query_data_name(self):
        query_data_name = self.cleaned_data.get('query_data_name')
        if not query_data_name:
            raise forms.ValidationError("Query Sensor name cannot be empty.")
        return query_data_name

    def clean_query_fields(self):
        query_fields = self.cleaned_data.get('query_fields')
        if not query_fields:
            raise forms.ValidationError("Query fields cannot be empty.")
        return query_fields

    def clean_query_where(self):
        query_where = self.cleaned_data.get('query_where')
        if not query_where:
            return ""
        return query_where