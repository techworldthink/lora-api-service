from django.db import models
from django .contrib.auth.models import User

class Api(models.Model):
    api_name = models.CharField(max_length=100)
    api_desc = models.CharField(max_length=200)
    api_status = models.IntegerField(default=1)
    api_createdon = models.DateTimeField(auto_now_add=True,blank=True)

class ApiUrl(models.Model):
    system_base_url = models.CharField(max_length=200)
    system_api_prefix = models.CharField(max_length=20)

class ApiPermission(models.Model):
    api = models.ForeignKey(Api,on_delete=models.CASCADE)
    user = models.ForeignKey(User,on_delete=models.CASCADE)

class ApiAccessHistory(models.Model):
    history_api = models.ForeignKey(Api,on_delete=models.CASCADE)
    history_user = models.ForeignKey(User,on_delete=models.CASCADE)
    history_isauthorized = models.IntegerField(default=0)
    history_accessdate = models.DateTimeField(auto_now_add=True,blank=True)