from django.urls import path
from . import views


app_name = "apiapp"


urlpatterns = [
    path('manage',views.api_manage,name="api_manage"),
    path('update', views.api_update,name="api_update"),
    path('delete/<int:api_id>', views.api_delete,name="api_delete"),
    path('status/<int:api_id>', views.api_status_toggle ,name="api_status_change"),

    path('devicesets',views.devicesets),
    path('devicesets/manage',views.devicesets_manage,name='devicesets_manage'),
    path('devicesets/update',views.devicesets_update),
    path('deviceset/delete/<int:id>',views.delete_deviceset),

    path('apis',views.apis),
    path('querysets',views.querysets),
    path('datasources',views.dataSources),
    path('measurements',views.measurements),
    path('fields',views.fields),
    path('distictvalues',views.distinctvalues),
    path('build',views.api_build,name="buildapi"),
    path('buildapi',views.build_api),
    path('queries/delete/<int:id>',views.delete_queries),

    path('permissions',views.permissions,name="permissions"),
    path('permissions/delete/<int:id>',views.delete_permissions),

    path('tokens',views.tokens,name="tokens"),
    path('tokens/create/<int:id>',views.token_generate,name="token_generate"),


]

