# Generated by Django 3.2.20 on 2023-09-14 15:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('datasourcesapp', '0005_queries_query_data_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='apisdevicesets',
            name='api_ds_device_sets',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='datasourcesapp.devicesets'),
        ),
    ]
