from django.shortcuts import render,redirect
from .models import DataSource
from django .contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from . import utils
from .forms import DataSourceForm



##################################################################################
#                                                                                #
#                                   VIEWS                                        # 
#                                                                                #
##################################################################################

# datasource listing and creation
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def datasource_create(request):
    if request.method == 'POST':
        form = DataSourceForm(request.POST)
        if form.is_valid():
            datasource = form.save(commit=False)

            if DataSource.objects.filter(
                    db_host=datasource.db_host,
                    db_port=datasource.db_port,
                    db_name=datasource.db_name,
                    db_user=datasource.db_user
            ).exists():
                messages.success(request, "Data source already exists")
            else:
                if utils.db_connection_check(datasource):
                    datasource.save()
                    messages.success(request, "Success! Datasource has been added.")
                else:
                    messages.error(request, "Connection failed")
                return redirect('adminapp:datasourceapp:datasource_create')

    else:
        form = DataSourceForm()

    datasource_list = DataSource.objects.values()

    return render(request, "datasourcesapp/add_datasource.html", {
        "datasources": datasource_list,
        "form": form,
    })

# update datasource details
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def datasource_update(request):
    if(request.method == 'POST'):
        datasource = DataSource(
            id = request.POST['db_id'],
            db_host = request.POST['db_host'],
            db_port = request.POST['db_port'],
            db_name = request.POST['db_name'],
            db_user = request.POST['db_user'],
            db_pass = request.POST['db_pass']
        )
        if(utils.db_connection_check(datasource)):
            try:
                datasource.save(update_fields=["db_host","db_port","db_name","db_user","db_pass"])
                messages.success(request, "Success! Datasource has been updated.")
            except:
                messages.error(request,"An error occurred. Please try again.")
        else:
            messages.success(request, "Connection failed.")

    
    return redirect('adminapp:datasourceapp:datasource_create')

# delete datasource
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def datasource_delete(request,datasource_id): 
    try:
        id = datasource_id
        datasource = DataSource.objects.get(pk=id)
        datasource.delete()
        messages.info(request, "Success! Datasource has been deleted.")
    except:
        messages.error(request,"An error occurred. Please try again.")

    return redirect('adminapp:datasourceapp:datasource_create')


# toggle datasource status
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def datasource_status_toggle(request,datasource_id):
    try:
        data_source = DataSource.objects.get(pk=datasource_id)
        data_source.db_status = not data_source.db_status
        data_source.save()
        messages.success(request, "Success! Datasource status updated.")
    except:
        messages.error(request,"An error occurred. Please try again.")

    return redirect('adminapp:datasourceapp:datasource_create')


# datasource connection check
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def connection_check(request,datasource_id):
    try:
        data_source = DataSource.objects.get(pk=datasource_id)
        if(utils.db_connection_check(data_source)):
            messages.success(request, "Success! Datasource connection ok.")
        else:
            messages.success(request, "Connection failed.")
    except Exception as e:
        messages.error(request,"An error occurred. Please try again.")

    return redirect('adminapp:datasourceapp:datasource_create')