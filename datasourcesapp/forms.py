from django import forms
from .models import DataSource

class DataSourceForm(forms.ModelForm):
    class Meta:
        model = DataSource
        fields = ['db_host', 'db_port', 'db_name', 'db_user', 'db_pass']
        
