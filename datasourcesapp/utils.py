from influxdb import InfluxDBClient
import threading
import time


def db_connection_check(data_source):
    try:
        db_client = InfluxDBClient(
            data_source.db_host, 
            data_source.db_port, 
            data_source.db_user, 
            data_source.db_pass, 
            data_source.db_name)

        def ping_with_timeout(client):
            try:
                result = client.ping()
                if(check_database_existence(client, db_client._database)):
                    return True 
                else:
                    time.sleep(6)
            except Exception as e:
                time.sleep(6)

        # Set a timeout for the ping operation
        # Set the timeout value in seconds
        timeout_seconds = 5

        ping_thread = threading.Thread(target=ping_with_timeout, args=(db_client,))
        ping_thread.start()
        ping_thread.join(timeout_seconds)

        if ping_thread.is_alive():
            # If the thread is still alive after the timeout, stop the ping
            #print("Ping operation timed out")
            # Close the InfluxDB connection to avoid potential issues
            db_client.close()
            return False

        else:
            db_client.close()
            return True

    except Exception as e:
        return False




def check_database_existence(client, target_database):
    databases = client.get_list_database()

    # Extracting only the database names from the list of dictionaries
    database_names = [db['name'] for db in databases]

    if target_database in database_names:
        # print(f"The database '{target_database}' exists.")
        return True
    else:
        # print(f"The database '{target_database}' does not exist.")
        return False


