from django.db import models
from apiapp.models import Api


class DataSource(models.Model):
    db_host = models.CharField(max_length=50,verbose_name='Host')
    db_port = models.CharField(max_length=6,blank=True,verbose_name='Port')
    db_user = models.CharField(max_length=50,verbose_name='User')
    db_pass = models.CharField(max_length=50,verbose_name='Password')
    db_name = models.CharField(max_length=50,verbose_name='Databse')
    db_status = models.IntegerField(default=1)
    db_createdon = models.DateTimeField(auto_now_add=True,blank=True)
    
class DeviceSet(models.Model):
    ds_name = models.CharField(max_length=100)
    ds_apis_id = models.ForeignKey(Api, on_delete=models.CASCADE, null=True)
    latitude = models.FloatField(null=True, default=None)
    longitude = models.FloatField(null=True, default=None)
    altitude = models.FloatField(null=True, default=None)
    is_combined = models.BooleanField(default=False)
    
class InfluxQuery(models.Model):
    query_data_name = models.CharField(max_length=100,null=True)
    query_measurement = models.CharField(max_length=100)
    # [field_1,field_2]
    query_fields = models.CharField(max_length=500)
    # [[field_1,">",value]]
    query_where = models.CharField(max_length=1000,null=True, default=None)
    query_limit = models.IntegerField(default=-1)

    query_datasource = models.ForeignKey(DataSource, on_delete=models.CASCADE)
    query_deviceset = models.ForeignKey(DeviceSet,on_delete=models.CASCADE,null=True)

 