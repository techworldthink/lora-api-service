from django.urls import path
from .views import *

app_name = 'datasourceapp'

urlpatterns = [
    path('', datasource_create,name="datasource_create"),
    path('update', datasource_update,name="datasource_update"),
    path('delete/<int:datasource_id>', datasource_delete,name="datasource_delete"),
    path('status/<int:datasource_id>', datasource_status_toggle ,name="datasource_status_toggle"),
    path('connection/<int:datasource_id>', connection_check ,name="connection_check"),
]