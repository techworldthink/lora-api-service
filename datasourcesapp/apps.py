from django.apps import AppConfig


class DatasourcesappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'datasourcesapp'
