#!/bin/bash
set -e
# Start installation
echo "=============================================";
echo "                INSTALLATION STARTED         ";
# Install MariaDB
echo "=============================================";
echo "                INSTALL - MARIADB            ";
echo "=============================================";
sudo apt install mariadb-server mariadb-client -y
sudo systemctl start mariadb
sudo mysql_secure_installation
# Create the database
echo "=============================================";
echo "                   DB CREATION               ";
echo "=============================================";
mysql -u root -p -e "create database lora_api_service"
# Install libmysqlclient-dev package
echo "=============================================";
echo "    INSTALL libmysqlclient-dev package       ";
echo "=============================================";
sudo apt-get install build-essential
sudo apt-get install libmysqlclient-dev -y

# Install Python virtual environment
echo "=============================================";
echo "    INSTALL python virtual environment       ";
echo "=============================================";
sudo apt install python3-venv -y
python3 -m venv env
source env/bin/activate
# Install Python dependencies
echo "=============================================";
echo "           INSTALL requirements.txt          ";
echo "=============================================";
pip install -r requirements.txt
# Install Gunicorn
echo "=============================================";
echo "               INSTALL gunicorn              ";
echo "=============================================";
pip install gunicorn
# Perform database migrations
echo "=============================================";
echo "    	     DB migration                  ";
echo "=============================================";
python3 manage.py makemigrations
python3 manage.py migrate
# Create a superuser
echo "=============================================";
echo "                CREATE SUPER USER            ";
echo "=============================================";
python3 manage.py createsuperuser
# Collect static files
echo "=============================================";
echo "             COLLECT STATIC FILES            ";
echo "=============================================";
python3 manage.py collectstatic
# Install Nginx
echo "=============================================";
echo "    		INSTALL nginx              ";
echo "=============================================";
sudo apt install nginx -y
# Change static files permissions
echo "=============================================";
echo "       static files permissions change       ";
echo "=============================================";
sudo usermod -a -G $USER www-data
sudo chown -R :www-data assets/
sudo systemctl restart nginx
# Create gunicorn.socket
echo "=============================================";
echo "             CREATE gunicorn.socket          ";
echo "=============================================";
sudo touch /etc/systemd/system/gunicorn.socket
# Create gunicorn.socket configuration
echo "
[Unit]
Description=gunicorn socket

[Socket]
ListenStream=/run/gunicorn.sock

[Install]
WantedBy=sockets.target
" | sudo tee /etc/systemd/system/gunicorn.socket

# Create gunicorn.service
echo "=============================================";
echo "             CREATE gunicorn.service         ";
echo "=============================================";

sudo touch /etc/systemd/system/gunicorn.service
# Create gunicorn.service configuration
echo "
[Unit]
Description=gunicorn daemon
Requires=gunicorn.socket
After=network.target

[Service]
User=$USER
Group=www-data
WorkingDirectory=$PWD
ExecStart=$PWD/env/bin/gunicorn \
          --access-logfile - \
          --workers 3 \
          --bind unix:/run/gunicorn.sock \
          lorapis.wsgi:application

[Install]
WantedBy=multi-user.target
" | sudo tee /etc/systemd/system/gunicorn.service

# Start and enable gunicorn.socket
sudo systemctl start gunicorn.socket
sudo systemctl enable gunicorn.socket
sudo systemctl status gunicorn.socket
# Check gunicorn.socket file
file /run/gunicorn.sock
# Enable, reload, and restart gunicorn.service
sudo systemctl enable gunicorn
sudo systemctl daemon-reload
sudo systemctl restart gunicorn
# Check the status of gunicorn.service
sudo systemctl status gunicorn

# Create the Nginx site configuration for the Django app
echo "=============================================";
echo "             CREATE lora-api site            ";
echo "  NOTE* : Edit configuration /etc/nginx/sites-available/lora-api  ";
echo "          to change domain name              ";              
echo "=============================================";

sudo touch /etc/nginx/sites-available/lora-api
# Create Nginx site configuration
echo "
server {
    listen 80;
    listen [::]:80;    server_name localhost;

    location = /favicon.ico { access_log off; log_not_found off; }
    
    location /static/ {
        alias $PWD/assets/;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/run/gunicorn.sock;
    }
}
" | sudo tee /etc/nginx/sites-available/lora-api

# Enable the Nginx site configuration
sudo ln -s /etc/nginx/sites-available/lora-api /etc/nginx/sites-enabled
# Check the Nginx configuration
sudo nginx -t
# Restart Nginx
sudo systemctl restart nginx
# Allow Nginx through the firewall
sudo ufw allow 'Nginx Full'
# Restart Nginx again
sudo systemctl restart nginx

echo "=============================================";
echo "                   Completed                 ";
echo "              Go to http://localhost/        ";
echo "=============================================";

