from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
import re


# Admin login form
class AdminLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


# User form
class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'password', 'email', 'first_name', 'last_name']

    def clean(self):
        cleaned_data = super().clean()
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')
        email = cleaned_data.get('email')
        first_name = cleaned_data.get('first_name')
        last_name = cleaned_data.get('last_name')

        if username == password:
            self.add_error('password', "An error has occurred! Can't use username as password.")

        if len(password) < 8:
            self.add_error('password', "Password minimum length should be 8 characters.")

        if User.objects.filter(username=username).exists():
            self.add_error('username', "Username already taken!")

        if User.objects.filter(email=email).exists():
            self.add_error('email', "Email already taken.")
        
        if not re.match(r'^[a-zA-Z0-9_]+$', first_name):
            self.add_error('first_name', "First Name can only contain alphabets.")

        if not re.match(r'^[a-zA-Z0-9_]+$', last_name):
            self.add_error('last_name', "First Name can only contain alphabets.")

        return cleaned_data