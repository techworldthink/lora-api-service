from django.http.response import HttpResponse
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django .contrib.auth.models import User,auth
from django .contrib import messages
from django.contrib.auth.hashers import make_password, check_password 
from apiapp.models import ApiUrl
from apiapp.models import Api
from datasourcesapp.models import DataSource
from django.utils import timezone
from apiapp.models import ApiAccessHistory
from django.db.models.functions import TruncMonth
from django.db.models import Count
import json
from .forms import AdminLoginForm
from .forms import UserForm



##################################################################################
#                                                                                #
#                                   VIEWS                                        # 
#                                                                                #
##################################################################################

# this view handle user login
def login(request):
    if request.method == "POST":
        form = AdminLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = auth.authenticate(username=username,password=password)

            if user is None:
                messages.info(request,'Invalid credentials!')
                return redirect('adminapp:login')

            elif(not user.is_superuser):
                messages.warning(request,"Invalid credentials!!")
                return redirect('adminapp:login')
            else:
                messages.warning(request,"Login successfull")
                auth.login(request,user)
                return redirect('adminapp:home')
        else:
            messages.warning(request,"Invalid form!")

    return render(request, 'login.html', {'form': AdminLoginForm()}) 

# logout
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def logout(request):
    auth.logout(request)
    return render(request,"index.html") 

# admin home page
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def home(request):
    return render(request,"admin/home.html") 

# admin dashboard 
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def dashboard(request):
    history_authorized = list(ApiAccessHistory.objects.filter(history_isauthorized=1).annotate(x=TruncMonth('history_accessdate')).values('x').annotate(y=Count('id')).order_by()) 
    history_unauthorized = list(ApiAccessHistory.objects.filter(history_isauthorized=0).annotate(x=TruncMonth('history_accessdate')).values('x').annotate(y=Count('id')).order_by()) 
  
    for index in range(len(history_authorized)):
        history_authorized[index]['x'] = history_authorized[index]['x'].date().month
    for index in range(len(history_unauthorized)):
        history_unauthorized[index]['x']= history_unauthorized[index]['x'].date().month

    return render(request,"admin/dashboard.html",{
        "users" : len(User.objects.all()),
        "apis" : len(Api.objects.all()),
        "datasources" : len(DataSource.objects.all()),
        "graph_authorized" : json.dumps(history_authorized, indent=4, sort_keys=True, default=str),
        "graph_unauthorized" : json.dumps(history_unauthorized, indent=4, sort_keys=True, default=str),
    }) 

# system settings manage view
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def settings(request):
    if(request.method == 'POST'):
        url = request.POST['system_url']
        prefix = request.POST['system_prefix']
        ApiUrl.objects.all().delete()
        apiurl_obj = ApiUrl(
            system_base_url = url,
            system_api_prefix = prefix
        )
        apiurl_obj.save()
        messages.success(request,"Success! System settings has been changed.")

    url_base = ""
    url_prefix = ""
    try:
        system_url = ApiUrl.objects.all().first()
        url_base = system_url.system_base_url
        url_prefix = system_url.system_api_prefix
    except Exception as e:
        messages.info(request,"Please update system base urls")
        url_base = "http://0.0.0.0/"
        url_prefix = "api/v1"

    
    return render(request,"admin/settings.html",{
        "url_base" : url_base,
        "url_prefix" : url_prefix
    }) 



##################################################################################
#                                                                                #
#                                 VIEWS  - Users                                 # 
#                                                                                #
##################################################################################

# user create & manage view
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def users(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.password = make_password(form.cleaned_data['password'])
            user.is_superuser = 0  # Assuming you don't want to create superusers here
            user.save()
            messages.success(request, "Success! New user account has been created.")
            return redirect('adminapp:users')  # Redirect to the users page or wherever you want
        else:
            messages.warning(request, "Invalid form data. Please check the details.")
    else:
        form = UserForm()

    user_list = User.objects.filter(is_superuser=0).values_list('first_name', 'last_name', 'username', 'email', 'id')
    return render(request, "admin/users.html", {
        "users": user_list,
        "form": form
    })

# delete user
@login_required()
@user_passes_test(lambda u: u.is_superuser)
def user_delete(request,id):
    try:
        user = User.objects.get(pk=id)
        if(user.is_superuser):
            messages.warning(request,"An error occurred. Please try again.")
        else:
            user.delete()
            messages.info(request,"Success! User has been removed.")
    except:
        messages.error(request,"An error occurred. Please try again.")
    
    return redirect('adminapp:users')