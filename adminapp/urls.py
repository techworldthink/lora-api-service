from django.urls import path,include
from . import views

app_name = 'adminapp'

urlpatterns = [
    path('', views.home,name="home"),
    path('login/', views.login,name="login"),
    path('logout/', views.logout,name="logout"),
    path('dashboard/', views.dashboard,name="dashboard"),
    path('settings/', views.settings,name="settings"),
    path('users/', views.users,name="users"),
    path('users/delete/<int:id>',views.user_delete,name="delete_user"),
    
    path('datasource/', include('datasourcesapp.urls',namespace='datasourceapp')),
    path('api/', include('apiapp.urls',namespace='apiapp')),   
]
